<?php

namespace Simplicity\FacebookBundle\Controller;

use Symfony\Component\HttpFoundation\RedirectResponse;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

abstract class FacebookController extends Controller {

  
  /**
    * Returns a RedirectResponse to the given URL.
    *
    * @param string  $url    The URL to redirect to
    * @param integer $status The status code to use for the Response
    *
    * @return RedirectResponse
    */
  public function redirect($url, $status = 302) {
    $request = $this->getRequest();
    $all = $request->query->all();
    $getQuery = '';
    foreach($all as $i=>$v) {
      $getQuery .= $i . '=' . $v . '&';
    }
    $redirectUrl = !empty($getQuery) ? $url . '?' . $getQuery : $url;
    return new RedirectResponse($redirectUrl, $status);
  }
  
  public function getFacebook() {
    return $this->get('simplicity.facebook');
  }
  
 public function getEm() {
   return $this->getDoctrine()->getManager(); 
 }
 
}

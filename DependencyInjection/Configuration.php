<?php

namespace Simplicity\FacebookBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html#cookbook-bundles-extension-config-class}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritDoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('simplicity_facebook');

        $rootNode
            ->children()
                ->arrayNode("apps")
                    ->requiresAtLeastOneElement()
                    ->useAttributeAsKey('name')
                    ->prototype('array')
                        ->children()
                
                            ->booleanNode ("devel")
                                ->defaultTrue()
                            ->end()
                
                            ->scalarNode("app_id")
                            ->isRequired()
                            ->cannotBeEmpty()
                            ->end()
                
                            ->scalarNode("secret")
                                ->isRequired()
                                ->cannotBeEmpty()
                            ->end()
                
                            ->scalarNode("app_token")
                                ->isRequired()
                                ->cannotBeEmpty()
                            ->end()
                
                            ->booleanNode("default")
                                ->defaultFalse()
                            ->end()

                            ->booleanNode("fileUpload")
                                ->defaultFalse()
                            ->end()
                            
                            ->scalarNode("canvas")
                                ->defaultFalse()
                            ->end()

                            ->scalarNode("fan_page_url_dev")
                                ->isRequired()
                                ->cannotBeEmpty()
                            ->end()

                            ->scalarNode("fan_page_url")
                                ->isRequired()
                                ->cannotBeEmpty()
                            ->end()
                
                    ->end()
                ->end()
            ->end();        
        
        return $treeBuilder;
    }
}

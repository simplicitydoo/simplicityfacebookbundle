<?php

namespace Simplicity\FacebookBundle\DependencyInjection;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\Loader;
use Symfony\Component\DependencyInjection\Reference;
use Symfony\Component\DependencyInjection\Definition;

/**
 * This is the class that loads and manages your bundle configuration
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html}
 */
class SimplicityFacebookExtension extends Extension
{
    /**
     * {@inheritDoc}
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        $loader = new Loader\YamlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
        $loader->load('services.yml');
        
        $session = new Reference('session');
        $em = new Reference('doctrine.orm.default_entity_manager');
        $logger = new Reference('logger');

        foreach ($config['apps'] as $name => $app) {
          if ($app['devel']) {
            $fanPageUrl = $app['fan_page_url_dev'];
          } else {
            $fanPageUrl = $app['fan_page_url'];
          }
          $facebookConfig = array(
              'appId' => $app['app_id'],
              'secret' => $app['secret'],
              'canvas' => $app['canvas'],
              'appToken' => $app['app_token'],
              'fileUpload' => $app['fileUpload'],
              'fan_page_url' => $fanPageUrl
          );

          $isDefault = $app['default'];

          $facebookDef = new Definition('Simplicity\FacebookBundle\Services\FacebookService', array($em, $session, $logger));
          $facebookDef->addArgument($facebookConfig);

          $container->setDefinition('simplicity.facebook.' . $name, $facebookDef);

          if ($isDefault) {
              $container->setAlias('simplicity.facebook', 'simplicity.facebook.' . $name);
          }
        }
        
        $analyticDef = new Definition('Simplicity\FacebookBundle\Services\AnalitycService', array($em, $session, $logger));
        $container->setDefinition('simplicity.analityc', $analyticDef);
    }
}

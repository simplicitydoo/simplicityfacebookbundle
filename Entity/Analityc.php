<?php

namespace Simplicity\FacebookBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Index;
use Simplicity\FacebookBundle\Entity\MemberInterface;
use Simplicity\HelperBundle\Entity\Entity;

/**
 * Analityc
 *
 * @ORM\Table(
 *            name="sim_analitycs",
 *            indexes={
 *                      @Index(name="session_id", columns={"session_id"})
 *                    }
 *            )
 * @ORM\Entity(repositoryClass="Simplicity\FacebookBundle\Repositories\AnalitycRepository")
 * @ORM\HasLifecycleCallbacks 
 */
class Analityc extends Entity
{

    /**
     * @var string
     *
     * @ORM\Column(name="session_id", type="string", length=128)
     */
    private $sessionId;

    /**
     * @var string
     *
     * @ORM\Column(name="ip_address", type="string", length=15)
     */
    private $ipAddress;

    /**
     * @var boolean
     *
     * @ORM\Column(name="no_like_no_user", type="boolean")
     */
    private $noLikeNoUser;
    /**
     * @var boolean
     *
     * @ORM\Column(name="has_like_no_user", type="boolean")
     */
    private $hasLikeNoUser;
    /**
     * @var boolean
     *
     * @ORM\Column(name="has_like_has_user", type="boolean")
     */
    private $hasLikeHasUser;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="fbuid", type="bigint", nullable=true)
     */
    private $fbUid;

    public function __construct() {
    }
    
    /**
     * Set sessionId
     *
     * @param string $sessionId
     * @return Analityc
     */
    public function setSessionId($sessionId)
    {
        $this->sessionId = $sessionId;
    
        return $this;
    }

    /**
     * Get sessionId
     *
     * @return string 
     */
    public function getSessionId()
    {
        return $this->sessionId;
    }

    /**
     * @ORM\PrePersist
     */
    public function prePersist()
    {
      parent::prePersist();
      
      $this->noLikeNoUser = false;
      $this->hasLikeNoUser = false;
      $this->hasLikeHasUser = false;
      
    }

    /**
     * Set ipAddress
     *
     * @param string $ipAddress
     * @return Analityc
     */
    public function setIpAddress($ipAddress)
    {
        $this->ipAddress = $ipAddress;
    
        return $this;
    }

    /**
     * Get ipAddress
     *
     * @return string 
     */
    public function getIpAddress()
    {
        return $this->ipAddress;
    }

    /**
     * Set noLikeNoUser
     *
     * @param boolean $noLikeNoUser
     * @return Analityc
     */
    public function setNoLikeNoUser($noLikeNoUser)
    {
        $this->noLikeNoUser = $noLikeNoUser;
    
        return $this;
    }

    /**
     * Get noLikeNoUser
     *
     * @return boolean 
     */
    public function getNoLikeNoUser()
    {
        return $this->noLikeNoUser;
    }

    /**
     * Set hasLikeNoUser
     *
     * @param boolean $hasLikeNoUser
     * @return Analityc
     */
    public function setHasLikeNoUser($hasLikeNoUser)
    {
        $this->hasLikeNoUser = $hasLikeNoUser;
    
        return $this;
    }

    /**
     * Get hasLikeNoUser
     *
     * @return boolean 
     */
    public function getHasLikeNoUser()
    {
        return $this->hasLikeNoUser;
    }

    /**
     * Set hasLikeHasUser
     *
     * @param boolean $hasLikeHasUser
     * @return Analityc
     */
    public function setHasLikeHasUser($hasLikeHasUser)
    {
        $this->hasLikeHasUser = $hasLikeHasUser;
    
        return $this;
    }

    /**
     * Get hasLikeHasUser
     *
     * @return boolean 
     */
    public function getHasLikeHasUser()
    {
        return $this->hasLikeHasUser;
    }
    
    /**
     * Set fbUid from $member
     *
     * @param use Simplicity\FacebookBundle\Entity\MemberInterface $member
     * @return EmailStatistic
     */
    public function setMember(MemberInterface $member)
    {
        $this->fbUid = $member->getId();
        return $this;
    }

    /**
     * Set fbUid
     *
     * @param integer $fbUid
     * @return EmailStatistic
     */
    public function setFbUid($fbUid)
    {
        $this->fbUid = $fbUid;
    
        return $this;
    }
    
    /**
     * Get member
     *
     * @return \Simplicity\Facebook\IntesangBundle\Entity\Member 
     */
    public function getFbUid()
    {
        return $this->fbUid;
    }
    
}
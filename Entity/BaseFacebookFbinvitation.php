<?php

namespace Simplicity\FacebookBundle\Entity;
use Doctrine\ORM\Mapping as ORM;

use Simplicity\HelperBundle\Entity\Entity;

abstract class BaseFacebookFbinvitation extends Entity
{

    /**
     * @var integer
     *
     * @ORM\Column(name="fbuid", type="bigint")
     */
    protected $fbuid;

    /**
     * @var integer
     *
     * @ORM\Column(name="invited", type="bigint")
     */
    protected $invited;

    /**
     * @var boolean
     *
     * @ORM\Column(name="accepted", type="boolean")
     */
    protected $accepted;


    public function __construct() {
    }

    /**
     */
    public function prePersist()
    {
      parent::prePersist();
      $this->accepted = false;
    }

    /**
     * Set fbuid
     *
     * @param integer $fbuid
     * @return Fbinvitation
     */
    public function setFbuid($fbuid)
    {
        $this->fbuid = $fbuid;
    
        return $this;
    }

    /**
     * Get fbuid
     *
     * @return integer 
     */
    public function getFbuid()
    {
        return $this->fbuid;
    }

    /**
     * Set invited
     *
     * @param integer $invited
     * @return Fbinvitation
     */
    public function setInvited($invited)
    {
        $this->invited = $invited;
    
        return $this;
    }

    /**
     * Get invited
     *
     * @return integer 
     */
    public function getInvited()
    {
        return $this->invited;
    }

    /**
     * Set accepted
     *
     * @param boolean $accepted
     * @return Fbinvitation
     */
    public function setAccepted($accepted)
    {
        $this->accepted = $accepted;
    
        return $this;
    }

    /**
     * Get accepted
     *
     * @return boolean 
     */
    public function getAccepted()
    {
        return $this->accepted;
    }
}

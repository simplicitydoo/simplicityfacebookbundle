<?php

namespace Simplicity\FacebookBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Simplicity\FacebookBundle\Entity\MemberInterface;

abstract class BaseFacebookMember implements MemberInterface {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint")
     * @ORM\Id
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="access_token", type="string", length=255, nullable=true)
     */
    protected $accessToken;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="access_token_date", type="datetime", nullable=true)
     */
    protected $accessTokenDate;
    
    /**
     * @var string
     *
     * @ORM\Column(name="fullname", type="string", length=255)
     */
    
    protected $fullName;
    
    /**
     * @var string
     *
     * @ORM\Column(name="first_name", type="string", length=255, nullable=true)
     */
    protected $firstName;

    /**
     * @var string
     *
     * @ORM\Column(name="last_name", type="string", length=255, nullable=true)
     */
    protected $lastName;

    /**
     * @var string
     *
     * 
     * @ORM\Column(name="email", type="string", length=128, nullable=true)
     */
    protected $email;

    /**
     * @var string
     *
     * @ORM\Column(name="gender", type="string", length=6, nullable=true)
     */
    protected $gender;


    /**
     * @var boolean
     *
     * @ORM\Column(name="is_active", type="boolean")
     */
    protected $active;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="create_date", type="datetime")
     */
    protected $createDate;
    
    public function __construct() {
    }

    /**
     * Set id
     *
     * @param integer $id
     * @return Member
     */
    public function setId($id)
    {
        $this->id = $id;
    
        return $this;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set accessToken
     *
     * @param string $accessToken
     * @return Member
     */
    public function setAccessToken($accessToken)
    {
        $this->accessToken = $accessToken;
    
        return $this;
    }

    /**
     * Get accessToken
     *
     * @return string 
     */
    public function getAccessToken()
    {
        return $this->accessToken;
    }

    /**
     * Set accessTokenDate
     *
     * @param \DateTime $accessTokenDate
     * @return Member
     */
    public function setAccessTokenDate($accessTokenDate)
    {
        $this->accessTokenDate = $accessTokenDate;
    
        return $this;
    }

    /**
     * Get accessTokenDate
     *
     * @return \DateTime 
     */
    public function getAccessTokenDate()
    {
        return $this->accessTokenDate;
    }

    /**
     * Set fullName
     *
     * @param string $fullName
     * @return Member
     */
    public function setFullName($fullName)
    {
        $this->fullName = $fullName;
    
        return $this;
    }

    /**
     * Get fullName
     *
     * @return string 
     */
    public function getFullName()
    {
        return $this->fullName;
    }

    /**
     * Set firstName
     *
     * @param string $firstName
     * @return Member
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;
    
        return $this;
    }

    /**
     * Get firstName
     *
     * @return string 
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * Set lastName
     *
     * @param string $lastName
     * @return Member
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;
    
        return $this;
    }

    /**
     * Get lastName
     *
     * @return string 
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return Member
     */
    public function setEmail($email)
    {
      
      if (empty($email)) {
        $email = '';
      }
      
      $this->email = $email;

      return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set gender
     *
     * @param string $gender
     * @return Member
     */
    public function setGender($gender)
    {
      if (empty($gender)) {
        $gender = '';
      }
     
      $this->gender = $gender;
    
      return $this;
    }

    /**
     * Get gender
     *
     * @return string 
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * Set change active state
     *
     * @return Member
     */
    public function changeActive() {
      $this->active = !$this->active;
      return $this;
    }
    
    /**
     * Get isActive
     *
     * @return boolean 
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set createDate
     *
     * @ORM\PrePersist
     */
    public function prePersist()
    {
        $this->createDate = new \DateTime();
        $this->active = true;
    }

    /**
     * Get createDate
     *
     * @return \DateTime 
     */
    public function getCreateDate()
    {
        return $this->createDate;
    }

    public function __toString() {
      return !empty($this->fullName) ? $this->fullName : '';
    }

    public function serialize() {
      $data = array(
          'id' => $this->id,
          'fullName' => $this->fullName,
      );
      return serialize($data);
    }
    
    public function unserialize($data) {
      $data = array(
          'id' => $this->id,
          'fullName' => $this->fullName,
      );
      $this->data = unserialize($data);
    }    
    
}

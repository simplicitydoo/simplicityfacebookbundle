<?php

namespace Simplicity\FacebookBundle\Entity;
use Doctrine\ORM\Mapping as ORM;

use Simplicity\HelperBundle\Entity\Entity;

abstract class BaseFacebookShare extends Entity {
 
    /**
     * @var integer
     *
     * @ORM\Column(name="fbuid", type="bigint")
     */
    protected $fbuid;

    /**
     * @var integer
     *
     * @ORM\Column(name="postid", type="bigint")
     */
    protected $postid;

    /**
     * @var string
     *
     * @ORM\Column(name="picture", type="string", length=255)
     */
    protected $picture;

    /**
     * @var string
     *
     * @ORM\Column(name="post_name", type="string", length=255)
     */
    protected $postName;

    /**
     * @var string
     *
     * @ORM\Column(name="post_description", type="string", length=255)
     */
    protected $postDescription;

    /**
     * @var string
     *
     * @ORM\Column(name="post_caption", type="string", length=255)
     */
    protected $postCaption;

    public function __construct() {
    }

    /**
     * Set fbuid
     *
     * @param integer $fbuid
     * @return Share
     */
    public function setFbuid($fbuid)
    {
        $this->fbuid = $fbuid;
    
        return $this;
    }

    /**
     * Get fbuid
     *
     * @return integer 
     */
    public function getFbuid()
    {
        return $this->fbuid;
    }

    /**
     * Set picture
     *
     * @param string $picture
     * @return Share
     */
    public function setPicture($picture)
    {
        $this->picture = $picture;
    
        return $this;
    }

    /**
     * Get picture
     *
     * @return string 
     */
    public function getPicture()
    {
        return $this->picture;
    }

    /**
     * Set postName
     *
     * @param string $postName
     * @return Share
     */
    public function setPostName($postName)
    {
        $this->postName = $postName;
    
        return $this;
    }

    /**
     * Get postName
     *
     * @return string 
     */
    public function getPostName()
    {
        return $this->postName;
    }

    /**
     * Set postDescription
     *
     * @param string $postDescription
     * @return Share
     */
    public function setPostDescription($postDescription)
    {
        $this->postDescription = $postDescription;
    
        return $this;
    }

    /**
     * Get postDescription
     *
     * @return string 
     */
    public function getPostDescription()
    {
        return $this->postDescription;
    }

    /**
     * Set postCaption
     *
     * @param string $postCaption
     * @return Share
     */
    public function setPostCaption($postCaption)
    {
        $this->postCaption = $postCaption;
    
        return $this;
    }

    /**
     * Get postCaption
     *
     * @return string 
     */
    public function getPostCaption()
    {
        return $this->postCaption;
    }

    /**
     * Get createDate
     *
     * @return \DateTime 
     */
    public function getCreateDate()
    {
        return $this->createDate;
    }

    /**
     * Set postid
     *
     * @param integer $postid
     * @return Share
     */
    public function setPostid($postid)
    {
        $this->postid = $postid;
    
        return $this;
    }

    /**
     * Get postid
     *
     * @return integer 
     */
    public function getPostid()
    {
        return $this->postid;
    }
    
    /**
     */
    public function prePersist()
    {
      parent::prePersist();
    }
    
    
}

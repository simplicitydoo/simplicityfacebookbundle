<?php

namespace Simplicity\FacebookBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Simplicity\FacebookBundle\Entity\MemberInterface;
use Simplicity\HelperBundle\Entity\Entity;

/**
 * EmailStatistic
 *
 * @ORM\Table(name="sim_email_statistics")
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks 
 */
class EmailStatistic extends Entity
{
    /**
     * @var integer
     *
     * @ORM\Column(name="fbuid", type="bigint")
     */
    private $fbUid;


    /**
     * Set fbUid
     *
     * @param integer $fbUid
     * @return EmailStatistic
     */
    public function setFbUid($fbUid)
    {
        $this->fbUid = $fbUid;
    
        return $this;
    }
    /**
     * Set fbUid from $member
     *
     * @param Simplicity\FacebookBundle\Entity\MemberInterface $member
     * @return EmailStatistic
     */
    public function setMember(MemberInterface $member)
    {
        $this->fbUid = $member->getId();
        return $this;
    }

    /**
     * Get member
     *
     * @return integer 
     */
    public function getFbUid()
    {
        return $this->fbUid;
    }
    
    /**
     * @ORM\PrePersist
     */
    public function prePersist()
    {
      parent::prePersist();
    }
    
}
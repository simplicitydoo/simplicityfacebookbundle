<?php

namespace Simplicity\FacebookBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Simplicity\FacebookBundle\Entity\MemberInterface;
use Simplicity\HelperBundle\Entity\Entity;

/**
 * MemberTracking
 *
 * @ORM\Table(name="sim_member_tracking")
 * @ORM\Entity(repositoryClass="Simplicity\FacebookBundle\Repositories\MemberTrackingRepository")
 * @ORM\HasLifecycleCallbacks 
 */
class MemberTracking extends Entity
{
    
    /**
     * @var integer
     *
     * @ORM\Column(name="fbuid", type="bigint")
     */
    private $fbUid;
    
    /**
     * @var string
     *
     * @ORM\Column(name="action", type="string", length=64)
     */
    private $action;

    /**
     * @var string
     *
     * @ORM\Column(name="controller", type="string", length=255)
     */
    private $controller;

    /**
     * @var string
     *
     * @ORM\Column(name="current_route", type="string", length=255)
     */
    private $currentRoute;

    /**
     * @var string
     *
     * @ORM\Column(name="next_route", type="string", length=255)
     */
    private $nextRoute;

    /**
     * Set action
     *
     * @param string $action
     * @return MemberTracking
     */
    public function setAction($action)
    {
        $this->action = $action;
    
        return $this;
    }

    /**
     * Get action
     *
     * @return string 
     */
    public function getAction()
    {
        return $this->action;
    }

    /**
     * Set controller
     *
     * @param string $controller
     * @return MemberTracking
     */
    public function setController($controller)
    {
        $this->controller = $controller;
    
        return $this;
    }

    /**
     * Get controller
     *
     * @return string 
     */
    public function getController()
    {
        return $this->controller;
    }

    /**
     * Set nextRoute
     *
     * @param string $nextRoute
     * @return MemberTracking
     */
    public function setNextRoute($nextRoute)
    {
        $this->nextRoute = $nextRoute;
    
        return $this;
    }

    /**
     * Get nextRoute
     *
     * @return string 
     */
    public function getNextRoute()
    {
        return $this->nextRoute;
    }

    /**
     * Set fbUid
     *
     * @param integer $fbUid
     * @return MemberTracking
     */
    public function setFbUid($fbUid)
    {
        $this->fbUid = $fbUid;
    
        return $this;
    }
    
    /**
     * Set fbUid from $member
     *
     * @param Simplicity\FacebookBundle\Entity\MemberInterface $member
     * @return MemberTracking
     */
    public function setMember(MemberInterface $member)
    {
        $this->fbUid = $member->getId();
        return $this;
    }

    /**
     * Get member
     *
     * @return \Simplicity\Facebook\IntesangBundle\Entity\Member 
     */
    public function getFbUid()
    {
        return $this->fbUid;
    }

    /**
     * Set currentRoute
     *
     * @param string $currentRoute
     * @return MemberTracking
     */
    public function setCurrentRoute($currentRoute)
    {
        $this->currentRoute = $currentRoute;
    
        return $this;
    }

    /**
     * Get currentRoute
     *
     * @return string 
     */
    public function getCurrentRoute()
    {
        return $this->currentRoute;
    }

    /**
     * @ORM\PrePersist
     */
    public function prePersist()
    {
      parent::prePersist();
    }
    
}
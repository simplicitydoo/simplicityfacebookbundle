SimplicityFacebookBundle
=====================

A Simplicity's bundle to encapulate Facebook SDK.

## Usage example
```php
<?php

namespace Acme\DemoBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class HomeController extends Controller {

    public function indexAction($facebookId, $facebookToken) {
        $FacebookService = $this->get("simplicity.facebook");
        
    }

}
?>
```

## Installation

Installation is a quick 3 step process:

1. Download SimplicityFacebookBundle using composer
2. Enable the Bundle
3. Configure your application's config.yml

### Step 1: Download SimplicityFacebookBundle using composer

Add SimplicityFacebookBundle in your composer.json:

```js
{
    "require": {
        "simplicity-projects/facebook-bundle": "1.*@dev"
    }
}
```

Now tell composer to download the bundle by running the command:

``` bash
$ php composer.phar update simplicity-projects/facebook-bundle
```

Composer will install the bundle to your project's `vendor/simplicity-projects` directory.

### Step 2: Enable the bundle

Enable the bundle in the kernel:

``` php
<?php
// app/AppKernel.php

public function registerBundles()
{
    $bundles = array(
        // ...
        new Simplicity\FacebookBundle\SimplicityFacebookBundle(),
    );
}
```
### Step 3: Configure your application's config.yml

Finally, add the following to your config.yml

``` yaml
# app/config/config.yml
simplicity_facebook:
    apps:
        fbapp:
            devel: true
            app_id: 12345
            secret: 34243
            default: true
            fileUpload: false
            canvas: ~
            fan_page_url_dev: your.dev.page.url
            fan_page_url: your_production.page.url
  
```

## Configuration
This bundle can be configured, and this is the list of what you can do :
- Create many apps.
- Define specific app_id / secret for each app.

**Note:**

```
You can retreive each connection using the container with "simplicity.facebook.[app_name]".

When you define a "default" connection. You can have a direct access to it with "simplicity.facebook".
```

<?php

namespace Simplicity\FacebookBundle\Services;

use Simplicity\FacebookBundle\Entity\Analityc;

class AnalitycService {
  private $em;
  private $session;
  
  const NO_LIKE_NO_USER = 1;
  const HAS_LIKE_NO_USER = 2;
  const HAS_LIKE_HAS_USER = 3;
  
  function __construct($em, $session) {
    $this->em = $em;
    $this->session = $session;
  }
  
  public function noLikeNoUser($ipAddress, $member = null) {
   $this->add($ipAddress, AnalitycService::NO_LIKE_NO_USER, $member);
  }

  public function hasLikeNoUser($ipAddress, $member) {
   $this->add($ipAddress, AnalitycService::HAS_LIKE_NO_USER, $member);
  }

  public function hasLikeHasUser($ipAddress, $member) {
   $this->add($ipAddress, AnalitycService::HAS_LIKE_HAS_USER, $member);
  }
  
  private function add($ipAddress, $type, $member = null) {
    $sessionId = $this->session->getId();
    
    $add = false;
    $analityc = $this->em->getRepository('SimplicityFacebookBundle:Analityc')->findOneBy(array('sessionId' => $sessionId));
    
    if (!$analityc) {
      $add = true;
    }
    
    
    if (is_object($analityc) AND $analityc->getIpAddress() != $ipAddress) {
      $add = true;
    }
    if ($add) {
      $analityc = new Analityc();
      $analityc->setSessionId($sessionId);
      $analityc->setIpAddress($ipAddress);
    }
    
    switch($type) {
      case AnalitycService::NO_LIKE_NO_USER:
        $analityc->setNoLikeNoUser(true);
        break;
      
      case AnalitycService::HAS_LIKE_NO_USER:
        $analityc->setHasLikeNoUser(true);
        break;
      
      case AnalitycService::HAS_LIKE_HAS_USER:
        $analityc->setHasLikeHasUser(true);
        break;
    }
    
    if (!empty($member)) {
      $analityc->setMember($member);
    }
    
    if ($add) {
      $this->em->persist($analityc);
    }
    
    $this->em->flush();
    
    return true;
  }
  
}

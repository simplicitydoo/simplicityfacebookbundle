<?php

namespace Simplicity\FacebookBundle\Services;

use Symfony\Component\HttpFoundation\Session\Session;

use Simplicity\FacebookBundle\Facebook\FacebookSessionPersistence;
use Simplicity\FacebookBundle\Entity\MemberInterface;

class FacebookService {
  private $em;
  private $session;
  private $facebook;
  
  private $logger;
  
  private $config;
  
  private $signedRequest;
  
  private $pageUrl;
  
  function __construct($em, Session $session, $logger, $config) {
    $this->em = $em;
    $this->session = $session;
    $this->config = $config;
    
    $this->facebook = new FacebookSessionPersistence($session, $config);
    
    $this->logger = $logger;
    
    $this->pageUrl = $config['fan_page_url'];
    
    $this->signedRequest();
  }
  
  public function getFacebook() {
    return $this->facebook;
  }
  
  private function signedRequest() {
    $signedRequest = $this->facebook->getSignedRequest();
    if (empty($signedRequest['page'])) {
      $signedRequest = $this->session->get('signed_request');
    } else {
      $this->session->set('signed_request', $signedRequest);
    }
    $this->signedRequest = $signedRequest;
  }
  
  function getSignedRequest() {
    return $this->signedRequest;
  }
  
  public function getAppUrl() {
    if (preg_match('~\?~', $this->pageUrl)) {
      return $this->pageUrl . '&sk=app_' . $this->facebook->getAppId() . '&';
    } else if (preg_match('~/\d+$~', $this->pageUrl)) {
      return $this->pageUrl . '?sk=app_' . $this->facebook->getAppId() . '&';
    } else {
      return $this->pageUrl . '/app_' . $this->facebook->getAppId() . '?';
    }
    
  }
  
  public function getPageUrl() {
    return $this->pageUrl;
  }
  
  public function getAppId() {
    return $this->facebook->getAppId();
  }
  
  public function getUser() {
	  return $this->facebook->getUser();
  }
  
  public function addInvites($fbUid, $invitations, $class) {
    if (is_array($invitations) && !empty($fbUid)) {
      foreach($invitations as $i) {
        try {
          $inv = new $class();
          if (is_object($fbUid)) {
            $inv->setMember($fbUid);
          } else {
            $inv->setFbuid($fbUid);
          }
          
          $inv->setInvited($i);
          $this->em->persist($inv);
          $this->em->flush();
        } catch (\Exception $e) {
        }
      }
    }
    return true;
  }
  
  /**
    * set AccessToken
    *
    * @param Simplicity\FacebookBundle\Entity\MemberInterface $member
    */
  public function setAccessToken(MemberInterface $member) {
    if ($member) {
      try {
          $this->facebook->setExtendedAccessToken();
          $accessToken = $this->session->get('_fos_facebook_fb_'  . $this->facebook->getAppId() . '_access_token');

          $this->facebook->setAccessToken($accessToken);

          $member->setAccessToken($accessToken);
          $member->setAccessTokenDate(new \DateTime('now'));
          $this->em->flush();
      } catch (\Exception $e) {
      }

    }
    
  }
  
  public function api($call, $method = '', $options = '') {
    if (!empty($options)) {
      return $this->facebook->api($call . '?access_token=' . $this->facebook->getAccessToken(), $method, $options);
    } else {
      return $this->facebook->api($call . '?access_token=' . $this->facebook->getAccessToken());
    }
    
  }
  
  public function notify($url, $msg, $fbUid) {
    $data = array(
        'href'=> $url,
        'template'=> $msg,
        'access_token'=> $this->facebook->getApplicationAccessToken()
    );
    try {
      $check = $this->facebook->getFacebook()->api('/' . $fbUid . '/notifications', 'POST', $data);
    } catch (\Exception $e) {
      $check = $e->getMessage();
    }
    return $check;
  }

  public function share() {
    return null;
  }

}
